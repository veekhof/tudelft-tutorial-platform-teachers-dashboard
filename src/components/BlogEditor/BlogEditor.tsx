import React from 'react';

const BlogEditor = () => {
    // The prototype of add tutorial page where user can add or edit tutorial
    return (
        <main className="container mx-auto flex flex-auto flex-row justify-between">
            <div className="relative w-1/4 text-white">
                sidebar
                <div className="absolute right-0 top-0 -z-10 h-full w-[300%] bg-secondary-navy"></div>
            </div>
            <div className="flex w-3/4 flex-col items-start pl-4">
                <div>Title</div>
                <div>TextField</div>
                <div>Div 3</div>
                <div>Div 4</div>
                <div>Div 5</div>
                <div>Title</div>
                <div>TextField</div>
                <div>Div 3</div>
                <div>Div 4</div>
                <div>Div 5</div>
                <div>Title</div>
                <div>TextField</div>
                <div>Div 3</div>
                <div>Div 4</div>
                <div>Div 5</div>
                <div>Title</div>
                <div>TextField</div>
                <div>Div 3</div>
                <div>Div 4</div>
                <div>Div 5</div>
                <div>Title</div>
                <div>TextField</div>
                <div>Div 3</div>
                <div>Div 4</div>
                <div>Div 5</div>
                <div>Title</div>
                <div>TextField</div>
                <div>Div 3</div>
                <div>Div 4</div>
                <div>Div 5</div>
                <div>Title</div>
                <div>TextField</div>
                <div>Div 3</div>
                <div>Div 4</div>
                <div>Div 5</div>
            </div>
        </main>
    );
};

export default BlogEditor;
